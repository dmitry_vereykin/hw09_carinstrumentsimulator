public class CarDemo //Car Instrument Simulator
{//Start of class Lab06
   public static void main(String[] args)
   {//Start of main
      FuelGauge fuel = new FuelGauge();
      
      // Create an Odometer object to work with the FuelGauge object.
      Odometer odometer = new Odometer(999980, fuel);
      
      // Fill the car up with gas.
      for (int i = 0; i < fuel.MAX_GALLONS; i++)
         fuel.incrementGallons();
         
      // Drive the car until it runs out of gas.
      while (fuel.getGallons() > 0)
      {
         // Drive a mile.
         odometer.incrementMileage();

         // Display the mileage.
         System.out.println("Mileage: " + odometer.getMileage());

         // Display the amount of fuel.
         System.out.println("Fuel level: " + fuel.getGallons() + 
                            " gallons");
         System.out.println("------------------------------");
      }
      
      System.out.println("End of trip.");
      
   }//End of main
}//End of class Lab06
